package ru.malakhov.tm.command.system;

import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.ArgumentConst;
import ru.malakhov.tm.constant.TerminalConst;

public final class AboutCommand extends AbstractCommand {

    @Override
    public String argument() {
        return ArgumentConst.ABOUT;
    }

    @Override
    public String name() {
        return TerminalConst.ABOUT;
    }

    @Override
    public String description() {
        return "Display developer info.";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("Name - Sergei Malakhov");
        System.out.println("Email - smalakhov2@rencredit.ru");
    }

}