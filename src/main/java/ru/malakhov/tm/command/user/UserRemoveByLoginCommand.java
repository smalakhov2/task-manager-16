package ru.malakhov.tm.command.user;

import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.TerminalConst;
import ru.malakhov.tm.model.Role;
import ru.malakhov.tm.util.TerminalUtil;

public class UserRemoveByLoginCommand extends AbstractCommand {

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.REMOVE_USER_BY_LOGIN;
    }

    @Override
    public String description() {
        return "Remove user by login.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeByLogin(login);
        System.out.println("[OK]");
    }

    @Override
    public Role[] role() {
        return new Role[] {Role.ADMIN};
    }

}