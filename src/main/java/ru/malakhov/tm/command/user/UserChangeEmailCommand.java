package ru.malakhov.tm.command.user;

import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.TerminalConst;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.util.TerminalUtil;

public final class UserChangeEmailCommand extends AbstractCommand {

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.CHANGE_EMAIL;
    }

    @Override
    public String description() {
        return "Change e-mail.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE E-MAIL]");
        System.out.println("ENTER NEW E-MAIL:");
        final String email = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().changeEmail(userId, email);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}