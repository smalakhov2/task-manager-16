package ru.malakhov.tm.command;

import ru.malakhov.tm.api.service.IServiceLocator;
import ru.malakhov.tm.model.Role;

public abstract class AbstractCommand {

    protected IServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract String argument();

    public abstract String name();

    public abstract String description();

    public abstract void execute();

    public Role[] role() {
        return null;
    }


}