package ru.malakhov.tm.command.project;

import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.TerminalConst;
import ru.malakhov.tm.entity.Project;
import ru.malakhov.tm.util.TerminalUtil;

public final class ProjectDisplayByIndexCommand extends AbstractCommand {

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.PROJECT_DISPLAY_BY_INDEX;
    }

    @Override
    public String description() {
        return "Display project by index.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = serviceLocator.getProjectService().findOneByIndex(userId, index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("[OK]");
    }

}