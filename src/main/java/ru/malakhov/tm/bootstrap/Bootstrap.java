package ru.malakhov.tm.bootstrap;

import ru.malakhov.tm.api.repository.ICommandRepository;
import ru.malakhov.tm.api.repository.IProjectRepository;
import ru.malakhov.tm.api.repository.ITaskRepository;
import ru.malakhov.tm.api.repository.IUserRepository;
import ru.malakhov.tm.api.service.*;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.service.*;
import ru.malakhov.tm.util.TerminalUtil;
import ru.malakhov.tm.exception.system.UnknownArgumentException;
import ru.malakhov.tm.exception.system.UnknownCommandException;
import ru.malakhov.tm.repository.CommandRepository;
import ru.malakhov.tm.repository.ProjectRepository;
import ru.malakhov.tm.repository.TaskRepository;
import ru.malakhov.tm.repository.UserRepository;
import ru.malakhov.tm.model.Role;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public final class Bootstrap implements IServiceLocator {

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ITaskRepository TaskRepository = new TaskRepository();

    private final ITaskService TaskService = new TaskService(TaskRepository);

    private final IProjectRepository ProjectRepository = new ProjectRepository();


    private final IProjectService ProjectService = new ProjectService(ProjectRepository);

    private final IAuthService authService = new AuthService(userService);

    private final ICommandService commandService = new CommandService(commandRepository);


    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    {
        putCommands(commandService.getCommandList());
    }

    private void putCommands(List<AbstractCommand> commandList) {
        for (AbstractCommand command: commandList) {
            if (command == null) return;
            command.setServiceLocator(this);
            commands.put(command.name(), command);
            arguments.put(command.argument(), command);
        }
    }

    private void createDefaultUsers(){
        userService.create("test", "test", "test@mail.ru");
        userService.create("admin","admin", Role.ADMIN);
    }

    private static void printHello() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
    }

    private void process() {
        while (true) {
            try {
                parseCommand(TerminalUtil.nextLine());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
            }
        }
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length < 1) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    private void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        final AbstractCommand argument = arguments.get(arg);
        if (argument == null) throw new UnknownArgumentException(arg);
        authService.checkRole(argument.role());
        argument.execute();
    }

    private void parseCommand(final String cmd) {
        if (cmd == null || cmd.isEmpty()) return;
        final AbstractCommand command = commands.get(cmd);
        if (command == null) throw new UnknownCommandException(cmd);
        authService.checkRole(command.role());
        command.execute();
    }

    public void run(final String[] args) {
        printHello();
        if (parseArgs(args)) System.exit(0);
        createDefaultUsers();
        process();
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ITaskService getTaskService() {
        return TaskService;
    }

    @Override
    public IProjectService getProjectService() {
        return ProjectService;
    }

}