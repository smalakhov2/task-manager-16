package ru.malakhov.tm.api.service;

import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.model.Role;

import java.util.List;

public interface IUserService {

    List<User> findAll();

    User create(String login, String password);

    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User findById(String id);

    User findByLogin(String login);

    User removeUser(User user);

    User removeById(String id);

    User removeByLogin(String login);

    String[] profile(String id);

    User changeEmail(String id, String email);

    User changePassword(String id, String password);

    User changeLogin(String id, String login);

    User changeFirstName(String id, String name);

    User changeMiddleName(String id, String name);

    User changeLastName(String id, String name);

    User lockUserByLogin(String login);

    User unlockUserByLogin(String login);

}