package ru.malakhov.tm.api.repository;

import ru.malakhov.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandRepository {

    List<AbstractCommand> getCommandList();

}